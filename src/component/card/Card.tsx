import './Card.scss'

export type CardListProps = {
  cardItems: CardProps[]
}

export type CardProps = {
  yearStart?: string,
  yearEnd?: string,
  title: string,
  content?: string,
  companyOrCampus: string
  imageSrc?: string
}

export function Card (cardProps : CardProps){
  const {companyOrCampus,title, content, imageSrc, yearStart, yearEnd} = cardProps;
  return(
    <div className={"card-container card-container-layout"}>
      <img className={"card-image"} src={imageSrc} alt=""/>
      <div className="card-title card-title-layout">{title}</div>
      <div className="card-corporation card-corporation-layout">{companyOrCampus}</div>
      {yearStart && yearEnd ?
        <div className="card-date card-date-layout">
          {yearStart} / {yearEnd}
        </div>:
        ''}
      <div className="card-content card-content-layout">{content}</div>
    </div>
  )
}

export function CardList({cardItems}: CardListProps){

  return(
    <div className={'card-list-layout'}>
      {cardItems.map((cardItem, index) =>
        <Card key={+index} {...cardItem}/>
      )}
    </div>
  )
}
