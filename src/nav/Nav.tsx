import './Nav.scss'
import logo from '../assets/logo.svg'
import {Link} from "react-router-dom";
import {MutableRefObject, useEffect, useRef} from "react";
import {useOutsideAlerter, useToggle, useWindowSize} from "../utils/hooks";

function useNonNullable <T>(val: T | null | undefined): NonNullable<T>{
  return val as NonNullable<T>;
}
export function Nav(){
  const outsideRef : MutableRefObject<HTMLDivElement | null> = useRef(null);
  const linksRef : MutableRefObject<HTMLUListElement | null> = useRef(null);
  useOutsideAlerter(outsideRef,() => {
    if(dropdownButton)
      toggleDropdownButton()
  }, linksRef);
  const [dropdownButton, toggleDropdownButton] = useToggle(false);

  const windowSize = useWindowSize();
  const [showDropDownButton, toggleShowDropDownButton] = useToggle(false);

  useEffect(() => {
    if(dropdownButton){
      outsideRef.current?.classList.add('active')
      linksRef.current?.classList.add('active')
    }else{
      outsideRef.current?.classList.remove('active')
      linksRef.current?.classList.remove('active')
    }
  }, [dropdownButton, toggleDropdownButton])

  useEffect(() => {
    if(windowSize.width < 991){
      if(!showDropDownButton){
        toggleShowDropDownButton()
      }
    }else{
      if(showDropDownButton){
        toggleShowDropDownButton()
      }
    }
  },[windowSize, showDropDownButton, toggleShowDropDownButton])


  return(
    <header>
      <Link to="/"><img className="logo" src={logo} alt=""/></Link>
      {showDropDownButton ?
        <div className={"nav-dropdown-button"}
             onClick={() => toggleDropdownButton()}
             ref={outsideRef}></div> : ''
      }
      <ul className="links" ref={linksRef} >
        <li><Link to="/experience">Mes Expériences</Link></li>
        <li><Link to="/formation">Mes Formations</Link></li>
        <li><Link to="/contact">Me Contacter</Link></li>
      </ul>
    </header>
  )
}
