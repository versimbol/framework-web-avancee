import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Error404} from "./pages/errors/error";
import {Home} from "./pages/home/Home";
import {Contact} from "./pages/contact/Contact";
import { Formation } from './pages/formation/Formation';
import {Experience} from "./pages/experience/Experience";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path={'/'} element={<App />} >
          <Route index element={<Home/>}/>
          <Route path={'/formation'} element={<Formation/>}/>
          <Route path={'/experience'} element={<Experience/>}/>
          <Route path={'/contact'} element={<Contact/>}/>
          <Route path={'*'} element={<Error404/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
