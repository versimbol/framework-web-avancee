import cardPicture from '../../assets/phil.png'
import './Home.scss'
import bpce from '../../assets/bpce.png'
import linxo from '../../assets/linxo.png'
import {CardList, CardProps} from "../../component/card/Card";
import React, {PropsWithChildren} from "react";


export function Home() {
  return (
    <>
      <section className={"main main-layout"}>
        <Presentation></Presentation>
      </section>
    </>
  )
}

function Presentation() {

  return (
    <div className="container">
      <div className="border-wrapper">
        <div className={"picture"}>
          <img src={cardPicture} alt=""/>
        </div>
      </div>
      <div className="content">
        <span className="text">Nom: SAID</span>
        <span className="text">Prénom: Faissoil </span>
        <span className="text">Étudiant</span>
        <span className="text">Alternant Dev FrontEnd</span>
        <span className="text">Entreprise: BPCE-SI</span>
      </div>
    </div>
  )
}