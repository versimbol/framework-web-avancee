import {CardList, CardProps} from "../../component/card/Card";
import React from "react";
import bpce from '../../assets/bpce.png'
import linxo from '../../assets/linxo.png'
import './Experience.scss'
const initCard: CardProps[] = [
  {
    title: 'Alternant développeur Web Front-end',
    companyOrCampus: 'BPCE-SI',
    yearStart: '2021',
    yearEnd: '2022',
    content: 'Création d\'outils de d\'analyse de dictionnaires à distance',
    imageSrc: bpce
  },
  {
    title: 'Stagiaire développeur Web Backend',
    companyOrCampus: 'Linxo',
    yearStart: '2020',
    yearEnd: '2020',
    content: 'Création d\'outils de d\'analyse de dictionnaires à distance',
    imageSrc: linxo
  }
]
export function Experience(){

  return(
    <>
      <div className="title title-layout">Expériences</div>
      <CardList cardItems={initCard}/>
    </>
  )
}