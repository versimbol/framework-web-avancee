import React, {FormEvent, useState} from "react";
import './Contact.scss'

export function Contact(){
  const firstNameState  = useState('');
  const lastNameState = useState('');
  const emailState = useState('');
  const textAreaState = useState('');

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    console.log(firstNameState[0], lastNameState[0], emailState[0], textAreaState[0])
  }

  return(
    <>
      <div className="contact-container">
        <form className={"form form-layout"} onSubmit={handleSubmit}>
          <Input labelName={"Nom"} labelId={"firstaname"} state={firstNameState}/>
          <Input labelName={"Prenom"} labelId={"lastname"} state={lastNameState}/>
          <Input labelName={"Email"} labelId={"email"} state={emailState} type={"email"} />
          <TextArea labelName={"Votre message"} labelId={"message"} state={textAreaState}/>
          <button className={"contact-button"} type={"submit"}>Submit</button>
        </form>
      </div>
    </>
  )
}



type LabeledProps = {
  labelName: string,
  labelId: string,
}
type StatefulProps<T> = {
  state: [T, (nextState: T) => void]
}

type InputProps = LabeledProps & StatefulProps<string> & {
  type?: React.HTMLInputTypeAttribute,
}

type TextAreaProps = LabeledProps & StatefulProps<string>

function TextArea({labelId, labelName, state}: TextAreaProps){
  const [value, setValue] = state;
  let ref: HTMLSpanElement;

  return (
    <div className={"text-area text-area-layout"}>
      <label className={"label"} htmlFor={labelId}>
        <span className={"textarea-default"} ref={(r) => r ? ref = r : null}>{labelName}</span>
        <textarea className={"text-area-element text-area-element-layout"}
                  value={value}
                  onChange={(event) => setValue(event.currentTarget.value)}
                  id={labelId}
                  required
                  onFocus={() => ref.classList.add('focused')}
                  onBlur={(e) => {
                    if(e.currentTarget.value)
                      return
                    ref.classList.remove('focused')
                  }}
        />
      </label>
    </div>
  )
}

function Input({labelName, type, labelId, state}: InputProps){
  const [value, setValue] = state;
  let ref: HTMLSpanElement;
  return(
    <div className={"input input-layout"}>
      <label className={"label"} htmlFor={labelId}>
        <span className={"input-default"} ref={r => r ? ref = r: null}>{labelName}</span>
        <input className={"input-element input-element-layout"}
               onChange={(event) => setValue(event.currentTarget.value)}
               value={value}
               id={labelId}
               type={type ? type : 'text'}
               required
               onFocus={() => ref.classList.add('focused')}
               onBlur={(e) => {
                 if(e.currentTarget.value)
                   return
                 ref.classList.remove('focused')
               }}
        />
      </label>
    </div>
  )
}



