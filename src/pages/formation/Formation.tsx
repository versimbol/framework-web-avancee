import './Formation.scss'
import {CardList, CardProps} from "../../component/card/Card";
import React from "react";
import amu from "../../assets/amu.png";
import ca from "../../assets/campus-academy.jpg";

const initCard: CardProps[] = [
  {
    title: 'Licence Informatique',
    companyOrCampus: 'Aix Marseille Université',
    yearStart: '2016',
    yearEnd: '2020',
    imageSrc: amu
  },
  {
    title: 'Master 1 Informatique, Spécialité Fiabilité Sécurité Informatique',
    companyOrCampus: 'Aix Marseille université',
    yearStart: '2020',
    yearEnd: '2021',
    imageSrc: amu
  },
  {
    title: 'Master 2 Informatique, InfoTech',
    companyOrCampus: 'Campus Academy',
    yearStart: '2021',
    yearEnd: '2022',
    imageSrc: ca
  }
]
export function Formation(){

  return(
    <>
      <div className="title title-layout">Formations</div>
      <CardList cardItems={initCard}/>
    </>  )
}
