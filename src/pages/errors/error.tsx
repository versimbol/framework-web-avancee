import {Link} from "react-router-dom";

export function Error404(){
  return(
    <>
      404
      <Link to={"/"}> Retourner à l'accueil</Link>
    </>
  )
}