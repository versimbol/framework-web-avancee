import React from 'react';
import './App.scss';
import {Nav} from "./nav/Nav";
import {Outlet} from "react-router-dom";

function App() {
  return (
    <>
      <Nav/>
      <Outlet/>
    </>
  );
}

export default App;
