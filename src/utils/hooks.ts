import {MutableRefObject, useCallback, useEffect, useState} from "react";

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState({
    width: window.innerWidth,
    height: window.innerHeight
  })

  useEffect(() => {
    function handleResize(){
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight
      });
    }

    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, [])
  return windowSize;
}

export const useToggle = (initialState: boolean): [boolean, () => void] => {
  const [state, setState] = useState(initialState);
  const toggle = useCallback(() => setState(state => !state),[]);
  return [state, toggle];
}

export const useOutsideAlerter = (ref: MutableRefObject<Element | null>, cb : () => void, refs?: MutableRefObject<Element | null>) => {
  useEffect(() => {
    function handleClickOutside(event: any){
      if(ref.current && !ref.current.contains(event.target) && refs && refs.current  &&!refs.current.contains(event.target)){
        cb()
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside)
  }, [ref, cb, refs])
}